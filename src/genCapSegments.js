"use strict";

/**
 * @description Creates caption segments for all captions and add delay in segments if followed by another segment
 * @use > node ./Utils/genCapSegment.js
 * @requires audio.json file to be at "../web-app/data/"
 * @outputs ../web-app/data/audio2.json
 * @param {int} segLength set this to be max char per caption-segment
 */

const fs = require("fs");
const _ = require("lodash");

const segLength = 100;
const scaleFactor = 0.87;

const wordAcrnms = ["PLUS", "FAFSA"]; // acronyms that are read as words

let whichOpened = 0;
let audioJson;

function attemptToOpen(filenames) {
  if (filenames.length) {
    whichOpened++;
    try {
      if (whichOpened > 1) console.log(`Attempting to open '${filenames[0]}'`);
      audioJson = require(filenames[0]);
    } catch {
      if (whichOpened > 1) console.log(`Could not find '${filenames[0]}'`);
      attemptToOpen(filenames.slice(1));
    }
  } else {
    throw new Error("MODULE_NOT_FOUND");
  }
}

try {
  attemptToOpen([
    `${__dirname}/audio.json`,
    `${__dirname}/../../../web-app/data/audio.json`,
    `${__dirname}/../../../web-app/data/audioORIGINAL.json`
  ]);
  main();
} catch (err) {
  if (err.code === "MODULE_NOT_FOUND" || err.message === "MODULE_NOT_FOUND")
    console.log("\nCould not find an audio json file to process.");
  else console.log(err);
}

function chopAtPunctuation(caption, punctuation, duration, del) {
  const charDur = duration / caption.length;
  const capSeg = [];
  let extraDelay = 0;

  // get all words that are all caps
  let acrnms = caption.split(" ").filter(word => word === word.toUpperCase());

  // remove punctutations
  acrnms = acrnms.map(word => word.replace(/[.,!\/#$%\^&\*{}=\-_`~()]/g, ""));
  // remove numbers
  acrnms = acrnms.filter(acrnm => isNaN(acrnm));
  // remove acronyms that are read as words
  acrnms = acrnms.filter(acrnm => wordAcrnms.indexOf(acrnm) < 0);

  acrnms.forEach(acrnm => {
    extraDelay += Math.round(acrnm.length * (charDur * 0.2));
  });

  // split captions without '!' or '.'
  if (punctuation == null) {
    let evenLength = caption.length / Math.ceil(caption.length / segLength) + 7;
    let choppedCap = caption.split(" ");
    let tmpSeg = "";

    for (let i = 0; i < choppedCap.length; i++) {
      let word = choppedCap[i];
      if ((tmpSeg + " " + word).length < evenLength) {
        tmpSeg = tmpSeg.concat(" ", word);
      } else {
        capSeg.push({ "caption-segment": tmpSeg.trim(), delay: del });
        del = Math.round(charDur * tmpSeg.length * scaleFactor) + extraDelay;
        tmpSeg = "";
        i--;
      }
    }

    capSeg.push({ "caption-segment": tmpSeg.trim(), delay: del });
    return capSeg;
  }

  const punctIndex = caption.indexOf(punctuation);

  if (
    punctIndex !== -1 &&
    punctIndex !== caption.length - 1 &&
    caption[punctIndex + 1] === " "
  ) {
    const segments = [];
    segments.push(
      caption.slice(0, punctIndex + 1),
      caption.slice(punctIndex + 2)
    );
    segments
      .filter(cap => cap.trim().length > 0)
      .forEach(cap => {
        if (cap.length > segLength) {
          capSeg.push(...genSegments(cap, charDur * cap.length, del));
          del =
            Math.round(
              capSeg[capSeg.length - 1]["caption-segment"].length *
                charDur *
                scaleFactor
            ) + extraDelay;
        } else {
          capSeg.push({ "caption-segment": cap, delay: del });
          del = Math.round(charDur * cap.length * scaleFactor) + extraDelay;
        }
      });
  }
  return capSeg;
}

function genSegments(caption, duration, delay = 0) {
  const words = caption.split(" ");

  words.forEach((word, index) => {
    // if word starts with $ then apply a comma thousand separator
    if (word.trim().length && word.charAt(0) === "$") {
      words[index] = word.replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    }
  });

  caption = words.join(" ");

  if (caption.length > segLength) {
    if (chopAtPunctuation(caption, ".", duration, delay).length > 0) {
      return chopAtPunctuation(caption, ".", duration, delay);
    } else if (chopAtPunctuation(caption, "!", duration, delay).length > 0) {
      return chopAtPunctuation(caption, "!", duration, delay);
    } else if (chopAtPunctuation(caption, "?", duration, delay).length > 0) {
      return chopAtPunctuation(caption, "?", duration, delay);
    } else return chopAtPunctuation(caption, null, duration, delay);
  } else return [{ "caption-segment": caption, delay: 0 }];
}

function main() {
  Object.keys(audioJson).forEach(language => {
    const audioClips = _.cloneDeep(audioJson[language]);

    Object.keys(audioClips).forEach(clip => {
      if (Array.isArray(audioClips[clip].caption)) {
        let caption = "";

        // collapse each caption segment into one string
        audioClips[clip].caption.forEach(captionSeg => {
          caption += `${captionSeg["caption-segment"]} `;
        });
        audioClips[clip].caption = `${caption}`.trim();
      }

      if (typeof audioClips[clip].caption === "string") {
        audioClips[clip].caption = genSegments(
          audioClips[clip].caption,
          audioClips[clip].duration
        );
      }
    });

    audioJson[language] = audioClips;
  });

  // find where to write output and write out new audio json with segments
  if (whichOpened === 1) {
    fs.writeFileSync(
      `${__dirname}/audio2.json`,
      JSON.stringify(audioJson, null, 2)
    );
    console.log(`Audio caption segments created in '${__dirname}/audio2.json'`);
  } else {
    fs.writeFileSync(
      `${__dirname}/../../../web-app/data/audio2.json`,
      JSON.stringify(audioJson, null, 2)
    );
    console.log(
      `Audio caption segments created in '${__dirname}/../../../web-app/data/audio2.json'`
    );
  }
}
