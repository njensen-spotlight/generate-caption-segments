'use strict';

/**
 * @description Combines caption segments to retrieve original audio json file
 * @use > node ./Utils/createOrigAudio.js
 * @requires audio.json file to be at "../web-app/data/"
 * @outputs ../web-app/data/audioORIGINAL.json
 */

const fs = require('fs');
const _ = require('lodash');

let whichOpened = 0;
let audioJson;

function attemptToOpen(filenames) {
  if (filenames.length) {
    whichOpened++;
    try {
      if (whichOpened > 1) console.log(`Attempting to open '${filenames[0]}'`);
      audioJson = require(filenames[0]);
    } catch {
      if (whichOpened > 1) console.log(`Could not find '${filenames[0]}'`);
      attemptToOpen(filenames.slice(1));
    }
  } else {
    throw new Error('MODULE_NOT_FOUND');
  }
}

try {
  attemptToOpen([
    `${__dirname}/audio.json`,
    `${__dirname}/../../../web-app/data/audio.json`,
  ]);
  main();
} catch (err) {
  if (err.code === "MODULE_NOT_FOUND" || err.message === "MODULE_NOT_FOUND")
    console.log("\nCould not find an audio json file to process.");
  else console.log(err);
}

function main() {
  Object.keys(audioJson).forEach((language) => {
    const audioClips = _.cloneDeep(audioJson[language]);
    Object.keys(audioClips).forEach((clip) => {
      let caption = '';

      if (Array.isArray(audioClips[clip].caption)) {
        // collapse each caption segment into one string
        for (let i = 0; i < audioClips[clip].caption.length; i++) {
          caption += `${audioClips[clip].caption[i]['caption-segment']} `;
        }
        audioClips[clip].caption = `${caption}`.trim();
      }
    });

    audioJson[language] = audioClips;
  });

  // find where to write output and write out new audio json without segments
  if (whichOpened === 1) {
    fs.writeFileSync(`${__dirname}/audioORIGINAL.json`, JSON.stringify(audioJson, null, 2));
    console.log(`Original audio created in '${__dirname}/audioORIGINAL.json'`);
  } else {
    fs.writeFileSync(
      `${__dirname}/../../../web-app/data/audioORIGINAL.json`,
      JSON.stringify(audioJson, null, 2)
    );
    console.log(`Original audio created in '${__dirname}/../../../web-app/data/audioORIGINAL.json'`);
  }
}
