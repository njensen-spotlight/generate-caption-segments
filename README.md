# Generate Caption Segments

This generates a new audio.json with updated caption segments and durations.

## Usage

### Installation
```
npm install generate-caption-segments --save-dev
```

### Scripts

Update package scripts for running scripts.
Add to package.json:
```
"createOrigAudio": "node node_modules/generate-caption-segments/src/createOrigAudio",
"genCapSegments": "node node_modules/generate-caption-segments/src/genCapSegments"
```

### Running

To create audioORIGINAL.json:
```
npm run createOrigAudio
```

To create audio2.json with caption segments:
```
npm run genCapSegments
```

### Replacements

Use these two files to update the captions and durations in the audio.json file.